﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_Application
{
    internal class Employee
    {
        private int _EmpNo;
        private string _Name;
        private string _Designation;
        private int _Salary;
        private int _Commission;
        private int _DeptNo;

        public Employee(int no, string name, string desg, int salary, int comm, int deptNo)
        {
            EmpNo = no;
            Name = name;
            Designation = desg;
            Salary = salary;
            Commission = comm;
            DeptNo = deptNo;
        }

        public int DeptNo
        {
            get { return _DeptNo; }
            set { _DeptNo = value; }
        }


        public int Commission
        {
            get { return _Commission; }
            set { _Commission = value; }
        }


        public int Salary
        {
            get { return _Salary; }
            set { _Salary = value; }
        }



        public string Designation
        {
            get { return _Designation; }
            set { _Designation = value; }
        }


        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }


        public int EmpNo
        {
            get { return _EmpNo; }
            set { _EmpNo = value; }
        }

        public double Calcluate_Total_Salary()
        {
            double total = Salary + Commission;
            return total;

        }

        public void showEmployees()
        {
            Console.WriteLine("No = {0}, Name = {1}, Desg ={2}, sal={3}, comm={4}, depNo={5}",
                                              EmpNo.ToString(),
                                              Name,
                                              Designation,
                                              Salary.ToString(),
                                              Commission.ToString(),
                                              DeptNo.ToString());
        }

    }
}
