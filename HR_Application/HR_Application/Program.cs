﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_Application
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Employee> employees = new List<Employee>();

            List<Department> department = new List<Department>();



            Boolean loop=true;
            while (loop)
            {
                Console.WriteLine("Enter choise 1:Add Employee and Department \n " +
                    "2:Show Employee and Department \n " +
                    "3:function to calculate total salary (Sal+Comm) of all employees \n " +
                    "4:display all employees of particular department \n " +
                    "5:function to calculate department wise count of employees \n " +
                    "6:function to calculate department wise average salary \n " +
                    "7:function to calculate department wise minimum salary \n " +
                    "0:Exit");
                int choice = Convert.ToInt32(Console.ReadLine());


                switch (choice)
                {
                    case 1:

                        Console.WriteLine(" Enter  Emp to add Employee  or  Dep to add Department");
                        string addchoice=Console.ReadLine();

                        if (addchoice == "Emp")
                        {
                            Console.WriteLine("Enter Emp No");
                            int No = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter Emp Name");
                            string Name = Console.ReadLine();
                            Console.WriteLine("Enter Emp Designation");
                            string Desg = Console.ReadLine();
                            Console.WriteLine("Enter Emp Salary");
                            int salary = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter Emp commission");
                            int comm = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter Emp DeptNo");
                            int DeptNo = Convert.ToInt32(Console.ReadLine());


                            employees.Add(new Employee(No, Name, Desg, salary, comm, DeptNo));
                        }
                        else
                        {
                            Console.WriteLine("Enter Dept No");
                            int No = Convert.ToInt32(Console.ReadLine());
                            Console.WriteLine("Enter Dep Name");
                            string Name = Console.ReadLine();
                            Console.WriteLine("Enter Dept Location");
                            string Location = Console.ReadLine();

                            department.Add(new Department(No, Name, Location));
                        }
                        Console.WriteLine("Employee or Department added sucessfuly");
                        Console.WriteLine("--------------------------------------------------------------------------");
                        Console.ReadLine();

                        break;

                    case 2:
                        Console.WriteLine(" Enter Emp  to show Employee  or  Dep to show Department");
                        string showchoice = Console.ReadLine();

                        if (showchoice == "Emp")
                        {
                            for (int i = 0; i < employees.Count; i++)
                            {
                                Employee emp = employees[i];
                                emp.showEmployees();

                            }
                        }
                        else
                        {
                            for (int i = 0; i < department.Count; i++)
                            {
                                Department dept = department[i];
                                dept.showDepartment();

                            }
                        }
                        
                        Console.WriteLine("--------------------------------------------------------------------------");
                        Console.ReadLine();

                        break;

                    case 3:
                        Console.WriteLine("Total salary of all employees");
                        for (int i = 0; i < employees.Count; i++)
                        {
                            Console.WriteLine("Emp Name = {0}, Total Salary= {1}", employees.ElementAt(i).Name, employees.ElementAt(i).Calcluate_Total_Salary());
                        }
                        Console.WriteLine("--------------------------------------------------------------------------");
                        break;

                    case 4:
                        Console.WriteLine(" Department wise employees list");
                        Console.WriteLine("Enter Department no");
                        int deptNo = Convert.ToInt32(Console.ReadLine());

                        for (int i = 0; i < employees.Count; i++)
                        {
                            if (deptNo == employees[i].DeptNo)
                            {
                                Console.WriteLine("No = {0}, Name = {1}, Desg ={2}, sal={3}, comm={4}, depNo={5}",
                                             employees.ElementAt(i).EmpNo.ToString(),
                                             employees.ElementAt(i).Name,
                                             employees.ElementAt(i).Designation,
                                             employees.ElementAt(i).Salary.ToString(),
                                             employees.ElementAt(i).Commission.ToString(),
                                             employees.ElementAt(i).DeptNo.ToString());
                            }
                            else
                            {
                                Console.WriteLine("Invalid Dept no");
                            }
                        }
                        Console.WriteLine("--------------------------------------------------------------------------");

                        break;

                    case 5:
                        Console.WriteLine(" Department wise employee count");
                        Console.WriteLine("Enter Department no");
                         deptNo = Convert.ToInt32(Console.ReadLine());
                        int count = 0;
                        
                        for (int i = 0; i< employees.Count; i++)
                        {
                            if (deptNo ==employees[i].DeptNo )
                                count++;
                        }
                        Console.WriteLine("Department no={0}, count={1} ",deptNo.ToString(),count.ToString());
                        Console.WriteLine("-----------------------------------------------------------------------");
                        Console.ReadLine();


                        break;
                    case 6:
                        Console.WriteLine(" Department wise average salary");
                        Console.WriteLine("Enter Department no");
                        deptNo = Convert.ToInt32(Console.ReadLine());
                        int Empsalary = 0;
                        int average = 0;
                        count= 0;
;
                        for (int i = 0;i< employees.Count; i++)
                        {
                            if(deptNo ==employees[i].DeptNo)
                            {
                                Empsalary = Empsalary + employees[i].Salary;
                                count++;
                            }

                        }
                        average = Empsalary / count;
                        Console.WriteLine("Average Salary of Department No {0} is {1}", deptNo.ToString(), average.ToString());
                        Console.WriteLine("-------------------------------------------------------------------------");
                        Console.ReadLine();


                        break;
                    case 7:
                        Console.WriteLine(" Department wise minimum salary");
                        Console.WriteLine("Enter Department no");
                        deptNo = Convert.ToInt32(Console.ReadLine());
                        int min = 0;
                        int a = 0;
                        
                        
                        for (int i = 0; i < employees.Count; i++)
                        {
                            if (deptNo == employees[i].DeptNo)
                            {
                               
                                a = a + 1;
                                if (a == 1)
                                {
                                    min = employees[i].Salary;
                                }
                                if (min > employees[i].Salary)
                                {
                                    min = employees[i].Salary;
                                }
                            }
                           

                        }
                        Console.WriteLine("minimum salary of deptN {0} is {1}", deptNo.ToString(), min.ToString());
                        Console.WriteLine("-------------------------------------------------------------------------");
                        Console.ReadLine();


                        break;



                    case 0:
                        loop = false;

                        break;


                    default:
                        break;
                }
            }

           
        }
        

    }
}
