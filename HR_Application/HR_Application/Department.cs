﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HR_Application
{
    internal class Department
    {
        public int DeptNo { get; set; }
        public string DeptName { get; set; }
        public string Location { get; set; }

        public Department(int No, string Name, string Location)
        {
            DeptNo = No;
            DeptName = Name;
            this.Location = Location;
        }


        public void showDepartment()
        {
            Console.WriteLine("DeptNo = {0}, DepartmentName = {1}, Location ={2}",

                               DeptNo.ToString(),
                               DeptName,
                               Location);


        }




    }
}
